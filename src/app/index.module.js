/* global malarkey:false, toastr:false, moment:false */
import API_CONFIG from './index.constants';

import config from './index.config';

import routerConfig from './index.route';

import runBlock from './index.run';
import MainController from './main/main.controller';
import RoomController from './room/room.controller';
import ModalController from './components/modal/modal.controller';
import ModalErrorController from './components/modal/modalError.controller';

import api from './services/api.service';

import dateCalendar from './components/calendar/date.calendar.directive.js';

angular.module('chiScheduler', [
  'ngCookies',
  'ngSanitize',
  'ui.router',
  'mwl.calendar',
  'ui.bootstrap',
  'firebase',
  'ngStorage'])

  .constant('toastr', toastr)
  .constant('moment', moment)
  .constant('API_CONFIG', API_CONFIG)

  .config(config)
  .config(routerConfig)

  .run(runBlock)
  .controller('MainController', MainController)
  .controller('RoomController', RoomController)
  .controller('ModalController', ModalController)

  .controller('ModalErrorController', ModalErrorController)

  .service('api', api)

  .directive('dateCalendar', dateCalendar);




