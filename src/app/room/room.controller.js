class MainController {
  constructor($scope, $rootScope, $q, $state, $modal, $localStorage, api, API_CONFIG, loadEvents) {
    'ngInject';
    let storage = $localStorage;
    //delete storage.user;
    this.api = api;
    this.$q = $q;
    this.$modal = $modal;
    this.modal = function() {
      let modalInstance = $modal.open({
        animation: this.animationsEnabled,
        templateUrl: 'app/components/modal/modal.html',
        controller: 'ModalController',
        controllerAs: 'modal'
      });
    };

    this.currentRoom = $state.params.pageNum;
    api.connection('room_' + this.currentRoom);
    // api.room.remove();
    this.calendarView = 'month';
    this.calendarDay = new Date();
    this.newEvent = {};
    this.toggle = function($event, field, event) {
      $event.preventDefault();
      $event.stopPropagation();
      event[field] = !event[field];
    };
    this.events = loadEvents || [];
    this.removeOldEvents();
    this.userData = storage.user;
    $rootScope.$on('userChanged', (event, data) => {
      this.userData = data;
    });
  }

  setBookinfo() {
    let params = {
      title: this.newEvent.title,
      startsAt: this.newEvent.startsAt,
      endsAt: this.newEvent.endsAt
    };

    this.checkDate(params).then(
      resolve => {
        let {title, startsAt, endsAt} = params;
        this.addNewBookInfo(title, startsAt, endsAt);
      },
      reject => {
        this.$modal.open({
          animation: this.animationsEnabled,
          templateUrl: 'app/components/modal/modal-error.html',
          controller: 'ModalErrorController',
          controllerAs: 'modalError'
        });
      }
    );
  }

  checkDate({ title, startsAt, endsAt }) {
    let defer = this.$q.defer();
    startsAt = startsAt.toString();
    endsAt = endsAt.toString();
    let available = true;

    this.api.room.once('value',
      snapshot => {
        let snap = snapshot.val() || {};

        Object.keys(snap).map((key) => {

          let snapStartsAt = moment(snap[key].startsAt);
          let snapEndsAt = moment(snap[key].endsAt);
          let newStartsAt = moment(startsAt);
          let newEndsAt = moment(endsAt);

          let startChecked = newStartsAt.isBetween(snapStartsAt, snapEndsAt);
          let endChecked = newEndsAt.isBetween(snapStartsAt, snapEndsAt);
          let globalChecked = snapStartsAt.isBetween(newStartsAt, newEndsAt) || snapEndsAt.isBetween(newStartsAt, newEndsAt);

          if (startChecked || endChecked || globalChecked) {
            available = false;
          }
          console.info('CHECK', available);
        });
        if (available) {
          defer.resolve();
        } else {
          defer.reject()
        }
      },
      errorObject => {
        console.log('The read failed: ' + errorObject.code);
      }
    );
    return defer.promise;
  }

  addNewBookInfo(title, startsAt, endsAt) {
    let titleName = title + " by " + this.userData.firstname + " " + this.userData.lastname;
    this.newEvent = {
      title: titleName,
      startsAt: startsAt.toString(),
      endsAt: endsAt.toString(),
      draggable: true,
      resizable: true,
      type: 'info'
    };
    this.api.room.push(this.newEvent);
    this.events.push(this.newEvent);
  }


  loadData() {
    let today = new Date;
    this.api.room.once('value',
      snapshot => {
        let snap = snapshot.val();
        Object.keys(snap).map((key) => {
          this.events.push(snap[key]);
        });
      },
      errorObject => {
        console.log('The read failed: ' + errorObject.code);
    });
  }

  removeOldEvents() {
    let currendDate = moment();
    this.events = this.events.filter((item) => {
      return currendDate.isBefore(item.endsAt);
    });
    this.api.room.set(this.events);
  }
}

export default MainController;
