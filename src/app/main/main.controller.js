class MainController {
  constructor($scope, $rootScope, $state, $modal, $localStorage) {
    'ngInject';
    let storage = $localStorage;
    this.modal = function() {
      let modalInstance = $modal.open({
        animation: this.animationsEnabled,
        templateUrl: 'app/components/modal/modal.html',
        controller: 'ModalController',
        controllerAs: 'modal'
      });
    };
    this.currentRoom = $state.params.pageNum;
    this.userData = storage.user;
    $rootScope.$on('userChanged', (event, data) => {
      this.userData = data;
    });
  }
}

export default MainController;