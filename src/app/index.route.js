function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      
      views: {
        '': {
          templateUrl: 'app/main/main.html',
          controller: 'MainController',
          controllerAs: 'vm'
        },
        'navbar@home': {
          templateUrl: 'app/main/navbar.html'
        }
      }
    })
    .state('room', {
      url: '/room/:pageNum',
      views: {
        '': {
          templateUrl: 'app/room/room_template.html',
          controller: 'RoomController',
          controllerAs: 'vm'
        },
        'navbar@room': {
          templateUrl: 'app/main/navbar.html'
        }
      },
      resolve: {
        loadEvents: function($q, $stateParams, api) {
          let defer = $q.defer();
          let today = new Date();
          let events = [];
          api.connection('room_' + $stateParams.pageNum);
          api.room.once('value', snapshot => {
              let snap = snapshot.val() || {};
              Object.keys(snap).map((key) => {
                events.push(snap[key]);
              });
              defer.resolve(events);
              console.log(events);
            },
            errorObject => {
              console.log('The read failed: ' + errorObject.code);
            });
          return defer.promise;
        }
      }
    })
    ;

  $urlRouterProvider.otherwise('/');
}

export default routerConfig;
