class api {
	constructor(API_CONFIG) {
		'ngInject';

		this.API_CONFIG = API_CONFIG;
		this.base_url = API_CONFIG.url_1757; 
		this.ref = new Firebase(this.base_url);
		this.users = new Firebase(this.base_url + 'users');
	}

	connection(url) {
	   this.room = new Firebase(this.base_url + url);
	}
	cleanEvents() {
		this.room.set({});
	}

	loadCustomUsersData() {	

        let usersData = [
          {
            id: 176,
            login: "alexander.miasoiedow",
            firstname: "Alexander",
            lastname: "Miasoiedow",
            mail: "alexander.miasoiedow@chisw.com",
            created_on: "2015-07-06T13:20:24Z",
            last_login_on: "2015-08-05T09:08:10Z"
          }, {
            id: 175,
            login: "alexander.seroshtan",
            firstname: "Alexander",
            lastname: "Seroshtan",
            mail: "alexander.seroshtan@chisw.com",
            created_on: "2015-07-06T07:38:41Z",
            last_login_on: "2015-07-30T10:37:04Z"
          }, {
            id: 172,
            login: "alexander.sytnyk",
            firstname: "Alexander",
            lastname: "Sytnyk",
            mail: "alexander.sytnyk@chisw.com",
            created_on: "2015-07-02T06:13:57Z",
            last_login_on: "2015-07-13T09:06:55Z"
          }, {
            id: 76,
            login: "alexandr.dekin",
            firstname: "Alexandr",
            lastname: "Dekin",
            mail: "alexandr.dekin@chisw.com",
            created_on: "2015-03-12T15:35:23Z",
            last_login_on: "2015-08-03T11:15:41Z"
          }, {
            id: 168,
            login: "alexandr.galichenko",
            firstname: "Alexandr",
            lastname: "Galichenko",
            mail: "alexandr.galichenko@chisw.com",
            created_on: "2015-06-25T09:48:50Z"
          }, {
            id: 169,
            login: "alexandr.kadaner",
            firstname: "Alexandr",
            lastname: "Kadaner",
            mail: "alexandr.kadaner@chisw.com",
            created_on: "2015-06-26T07:16:19Z",
            last_login_on: "2015-06-26T08:55:49Z"
          }, {
            id: 130,
            login: "alexandr.kolosov",
            firstname: "Alexandr",
            lastname: "Kolosov",
            mail: "alexandr.kolosov@chisw.com",
            created_on: "2015-03-30T11:16:42Z",
            last_login_on: "2015-06-30T08:40:29Z"
          }, {
            id: 124,
            login: "alexandr.okorokov",
            firstname: "Alexandr",
            lastname: "Okorokov",
            mail: "alexandr.okorokov@chisw.com",
            created_on: "2015-03-26T12:29:49Z",
            last_login_on: "2015-08-06T08:19:36Z"
          }, {
            id: 148,
            login: "alexandr.ripa",
            firstname: "Alexandr",
            lastname: "Ripa",
            mail: "alexandr.ripa@chisw.com",
            created_on: "2015-05-08T13:26:04Z",
            last_login_on: "2015-07-29T07:22:36Z"
          }, {
            id: 178,
            login: "alexandr.stepanenko",
            firstname: "Alexandr",
            lastname: "Stepanenko",
            mail: "alexandr.stepanenko@chisw.com",
            created_on: "2015-06-26T11:42:00Z",
            last_login_on: "2015-08-06T06:04:00Z"
          }, {
            id: 53,
            login: "alexandr.svystun",
            firstname: "Alexandr",
            lastname: "Svystun",
            mail: "alexandr.svystun@chisw.com",
            created_on: "2015-02-27T16:07:13Z",
            last_login_on: "2015-08-04T19:59:19Z"
          }, {
            id: 139,
            login: "alexandr.zhitnik",
            firstname: "Alexandr",
            lastname: "Zhitnik",
            mail: "alexandr.zhitnik@chisw.com",
            created_on: "2015-04-01T15:50:57Z",
            last_login_on: "2015-08-03T17:33:59Z"
          }, {
            id: 146,
            login: "alexey.bachevych",
            firstname: "Alexey",
            lastname: "Bachevych",
            mail: "alexey.bachevych@strikersoft.com",
            created_on: "2015-04-24T14:47:04Z",
            last_login_on: "2015-04-24T15:24:50Z"
          }, {
            id: 113,
            login: "alexey.shvedko",
            firstname: "Alexey",
            lastname: "Shvedko",
            mail: "alexey.shvedko@chisw.com",
            created_on: "2015-03-17T15:22:48Z",
            last_login_on: "2015-07-20T10:39:06Z"
          }, {
            id: 89,
            login: "alexey.stepchenko",
            firstname: "Alexey",
            lastname: "Stepchenko",
            mail: "alexey.stepchenko@chisw.com",
            created_on: "2015-03-17T10:55:26Z",
            last_login_on: "2015-08-04T06:50:39Z"
          }, {
            id: 43,
            login: "alexey.stepchenkov",
            firstname: "Alexey",
            lastname: "Stepchenkov",
            mail: "alexey.stepchenkov@chisw.com",
            created_on: "2015-02-26T14:23:17Z",
            last_login_on: "2015-08-03T11:58:07Z"
          }, {
            id: 42,
            login: "alina.zimina",
            firstname: "Alina",
            lastname: "Zimina",
            mail: "alina.zimina@chisw.com",
            created_on: "2015-02-26T14:19:53Z",
            last_login_on: "2015-08-06T08:33:18Z"
          }, {
            id: 155,
            login: "anastasia.kolomiets",
            firstname: "Anastasia",
            lastname: "Kolomiets",
            mail: "anastasia.kolomiets@chisw.com",
            created_on: "2015-05-28T17:54:30Z",
            last_login_on: "2015-08-04T15:44:41Z"
          }, {
            id: 13,
            login: "andrew.tutunnik",
            firstname: "Andrew",
            lastname: "Tutunnik",
            mail: "andrew.tutunnik@chisw.com",
            created_on: "2015-02-19T13:30:47Z",
            last_login_on: "2015-08-04T07:33:25Z"
          }, {
            id: 94,
            login: "andrey.dmytrenko",
            firstname: "Andrey ",
            lastname: "Dmytrenko",
            mail: "andrey.dmytrenko@chisw.com",
            created_on: "2015-03-17T13:45:03Z",
            last_login_on: "2015-07-17T09:01:16Z"
          }, {
            id: 47,
            login: "andrey.kobka",
            firstname: "Andrey",
            lastname: "Kobka",
            mail: "andrey.kobka@chisw.com",
            created_on: "2015-02-26T16:55:09Z",
            last_login_on: "2015-08-06T08:38:43Z"
          }, {
            id: 64,
            login: "andrey.kozhurov",
            firstname: "Andrey",
            lastname: "Kozhurov",
            mail: "andrey.kozhurov@chisw.com",
            created_on: "2015-03-03T09:26:32Z",
            last_login_on: "2015-08-06T08:28:50Z"
          }, {
            id: 14,
            login: "andrey.kudryashov",
            firstname: "Andrey",
            lastname: "Kudryashov",
            mail: "andrey.kudryashov@chisw.com",
            created_on: "2015-02-20T15:53:02Z",
            last_login_on: "2015-08-05T12:58:38Z"
          }, {
            id: 77,
            login: "andrey.mygal",
            firstname: "Andrey",
            lastname: "Mygal",
            mail: "andrey.mygal@chisw.com",
            created_on: "2015-03-12T15:35:47Z",
            last_login_on: "2015-08-05T14:28:41Z"
          }, {
            id: 78,
            login: "andrey.shatalov",
            firstname: "Andrey",
            lastname: "Shatalov",
            mail: "andrey.shatalov@chisw.com",
            created_on: "2015-03-12T15:36:09Z",
            last_login_on: "2015-07-28T08:21:59Z"
          }
        ];
        this.users.set(usersData);
	}
}

export default api;