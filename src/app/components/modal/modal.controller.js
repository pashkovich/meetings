class ModalController {
    constructor($rootScope, $modal, $localStorage, api, $modalInstance) {
        'ngInject';
        this.storage = $localStorage;
        this.modal = $modal;
        this.modalInstance = $modalInstance;
        this.rootScope = $rootScope;
        this.userArr = [];
        let states = [];
        api.ref.once('value', snapshot => {
            let snap = snapshot.val() || {};
            this.userArr = snap.users;
            let len = this.userArr.length;                
            for (let i = 0; i < len; i++) {
                states.push(this.userArr[i].firstname + " " + this.userArr[i].lastname);
            }
        });
        this.states = states;
    }

    onSelect($item) {
        let _fName = $item.split(" ")[0],
            _lName = $item.split(" ")[1];
        this._unicUser = this.findWhere("firstname", _fName, "lastname", _lName, this.userArr);
    }

    addUser() {
        this.storage.user = this._unicUser;
        this.modalInstance.dismiss('cancel');
        this.rootScope.$emit('userChanged', this.storage.user);
    }

    findWhere(propertyName, propertyValue, propertyName2, propertyValue2, collection) {
      let len = collection.length;
      for (let i = 0; i < len; i++) {
        if (collection[i][propertyName] == propertyValue && collection[i][propertyName2] === propertyValue2) {
          return collection[i];
        }
      }
      return null;
    }
    
}
export default ModalController;