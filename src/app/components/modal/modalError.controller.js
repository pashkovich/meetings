class ModalErrorController {
  constructor($modal, $modalInstance) {
    'ngInject';
    this.modal = $modal;
    this.modalInstance = $modalInstance;
  }

  onClose() {
    this.modalInstance.dismiss('cancel');
  }
}

export default ModalErrorController;
